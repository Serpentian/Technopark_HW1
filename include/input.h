// Copyright (c) 2022 Nikita Zheleztsov
//
// Distributed under the GNU GPLv3 software license, see the accompanying
// file LICENSE or visit <https://www.gnu.org/licenses/gpl-3.0.en.html>

#ifndef INCLUDE_INPUT_H_
#define INCLUDE_INPUT_H_

#include <stdio.h>

char *input_string(FILE *input);

#endif  // INCLUDE_INPUT_H_
