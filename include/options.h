// Copyright (c) 2022 Nikita Zheleztsov
//
// Distributed under the GNU GPLv3 software license, see the accompanying
// file LICENSE or visit <https://www.gnu.org/licenses/gpl-3.0.en.html>

#ifndef INCLUDE_OPTIONS_H_
#define INCLUDE_OPTIONS_H_

#include <stdio.h>

typedef struct {
  FILE *output;
  char *tag_string;
} options_t;

#endif  // INCLUDE_OPTIONS_H_
