// Copyright (c) 2022 Nikita Zheleztsov
//
// Distributed under the GNU GPLv3 software license, see the accompanying
// file LICENSE or visit <https://www.gnu.org/licenses/gpl-3.0.en.html>

#ifndef INCLUDE_JSONIFY_H_
#define INCLUDE_JSONIFY_H_

#include <string.h>

#include "error.h"
#include "options.h"

//---- JSONIFY INTERFACE ----//
err_code_t jsonify(options_t *options);

#endif  // INCLUDE_JSONIFY_H_
