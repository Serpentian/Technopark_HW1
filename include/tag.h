// Copyright (c) 2022 Nikita Zheleztsov
//
// Distributed under the GNU GPLv3 software license, see the accompanying
// file LICENSE or visit <https://www.gnu.org/licenses/gpl-3.0.en.html>

#ifndef INCLUDE_TAG_H_
#define INCLUDE_TAG_H_

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

typedef struct {
  char *attr_name;
  char *attr_value;
} attribute_t;

typedef struct {
  size_t attributes_num;
  size_t attributes_cap;
  attribute_t *attributes;
} attributes_arr_t;

typedef enum { OPENING, CLOSING } feature_t;

typedef struct {
  char *name;
  feature_t feature;
  attributes_arr_t attr_arr;
} tag_t;

typedef enum { SPACE_MODE, QUOTES_MODE } extract_name_mode_t;

//--- Tag interface ---//
void free_tag(tag_t *tag);
tag_t *construct_tag(const char *string);
void print_json(FILE *out, tag_t *tag);

// For inside use only, to public interface for testing
feature_t extract_feature(char **iter);
int extract_attributes(attributes_arr_t *arr, char *iter, char *end_iter);
int extract_one_attr(attribute_t *attr, char **iter, const char *end_iter);
int verify_tag(const char *str, char **begin_iter, char **end_iter);
char *extract_name(char **iter, const char *end_iter, extract_name_mode_t mode);
size_t find_name(char **iter, char **begin_name, const char *end_iter,
                 extract_name_mode_t mode);

#endif  // INCLUDE_TAG_H_
