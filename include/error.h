// Copyright (c) 2022 Nikita Zheleztsov
//
// Distributed under the GNU GPLv3 software license, see the accompanying
// file LICENSE or visit <https://www.gnu.org/licenses/gpl-3.0.en.html>

#ifndef INCLUDE_ERROR_H_
#define INCLUDE_ERROR_H_

#define ERR_UNKNOWN "UNKNOWN ERROR"

//---- XX_MACRO_IDIOM ----//
#define ERROR_CODES              \
  XX(0, SUCCESS)                 \
  XX(1, PARSING_ERROR)           \
  XX(2, MEMORY_ALLOCATION_ERROR) \
  XX(3, INPUT_STRING_ERROR)

#define XX(error_code, error_msg) error_msg = (error_code),
typedef enum { ERROR_CODES } err_code_t;
#undef XX

const char *get_err_msg(err_code_t error_code);

#endif  // INCLUDE_ERROR_H_
