// Copyright (c) 2022 Nikita Zheleztsov
//
// Distributed under the GNU GPLv3 software license, see the accompanying
// file LICENSE or visit <https://www.gnu.org/licenses/gpl-3.0.en.html>

#include "input.h"

#include <stdio.h>
#include <stdlib.h>

char *input_string(FILE *input) {
  struct {
    char *str;
    size_t str_size;
    size_t str_cap;
  } buffer = {NULL, 0, 0};

  if (input) {
    // NOLINTNEXTLINE (*-id-dependent-backward-branch)
    for (int c = fgetc(input); c != EOF && c != '\n'; c = fgetc(input)) {
      if (buffer.str_size + 1 >= buffer.str_cap) {
        size_t new_cap = buffer.str_cap ? buffer.str_cap * 2 : 1;
        char *temp = (char *)realloc(buffer.str, sizeof(char) * (new_cap + 1));

        if (temp) {
          buffer.str = temp;
          buffer.str_cap = new_cap;
        } else {
          free(buffer.str);
          return NULL;
        }
      }

      buffer.str[buffer.str_size++] = (char)c;
      buffer.str[buffer.str_size] = '\0';
    }
  }

  return buffer.str;
}
