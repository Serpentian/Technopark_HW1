// Copyright (c) 2022 Nikita Zheleztsov
//
// Distributed under the GNU GPLv3 software license, see the accompanying
// file LICENSE or visit <https://www.gnu.org/licenses/gpl-3.0.en.html>

#include "jsonify.h"

#include <stdlib.h>
#include <unistd.h>

#include "tag.h"

err_code_t jsonify(options_t *options) {
  tag_t *tag = NULL;
  if (!(tag = construct_tag(options->tag_string))) {
    return PARSING_ERROR;
  }

  print_json(options->output, tag);

  free_tag(tag);
  return SUCCESS;
}
