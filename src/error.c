// Copyright (c) 2022 Nikita Zheleztsov
//
// Distributed under the GNU GPLv3 software license, see the accompanying
// file LICENSE or visit <https://www.gnu.org/licenses/gpl-3.0.en.html>

#include "error.h"

#define XX(error_code, error_message) \
  case error_code:                    \
    return #error_message;

const char* get_err_msg(err_code_t error_code) {
  switch (error_code) {
    ERROR_CODES
    default:
      return ERR_UNKNOWN;
  }
}

#undef XX
