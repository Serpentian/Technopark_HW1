// Copyright (c) 2022 Nikita Zheleztsov
//
// Distributed under the GNU GPLv3 software license, see the accompanying
// file LICENSE or visit <https://www.gnu.org/licenses/gpl-3.0.en.html>

#include <errno.h>
#include <getopt.h>
#include <libgen.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

#include "error.h"
#include "input.h"
#include "jsonify.h"
#include "options.h"

#define OPTSTR "o:p:h"
#define ERR_SYSTEM "System error: "
#define ERR_JSONIFY "Jsonify error: %s\n"
#define DEFAULT_PROGNAME "jsonify_tag"
#define USAGE_FMT "%s [-p tagstring] [-o outputfile] [-h]\n"

// NOLINTNEXTLINE (*-avoid-non-const-global-variables)
extern int opterr, opterr;

void help(char *progname, int opt);

int main(int argc, char *argv[]) {
  options_t options = {stdout, NULL};

  opterr = 0;
  int opt = 0;
  // NOLINTNEXTLINE: Allow not thread safe function getopt
  while ((opt = getopt(argc, argv, OPTSTR)) != EOF) {
    switch (opt) {
      case 'o':
        if (!(options.output = fopen(optarg, "we"))) {
          perror(ERR_SYSTEM);
          return EXIT_FAILURE;
        }
        break;

      case 'p':
        options.tag_string = optarg;
        break;

      case 'h':
      default:
        help(basename(argv[0]), opt);
        return EXIT_FAILURE;
    }
  }

  //--- Getting tag from stdin if needed ---//
  char *temp_input = NULL;
  if (!options.tag_string) {
    printf("Enter the tag to convert: ");
    if ((temp_input = input_string(stdin))) {
      options.tag_string = temp_input;
    } else {
      fprintf(stderr, ERR_JSONIFY, get_err_msg(INPUT_STRING_ERROR));
      return EXIT_FAILURE;
    }
  }

  //--- Converting tag to json format ---//
  err_code_t err = jsonify(&options);
  if (err != SUCCESS) {
    fprintf(stderr, ERR_JSONIFY, get_err_msg(err));
    free(temp_input);
    return EXIT_FAILURE;
  }

  free(temp_input);
  return EXIT_SUCCESS;
}

void help(char *progname, int opt) {
  if (opt == '?') {
    fprintf(stderr, "Unknown option encountered: '-%c'\n", optopt);
  } else if (opt == ':') {
    fprintf(stderr, "Missing argument for option '-%c'\n", optopt);
  }

  fprintf(stderr, USAGE_FMT, progname ? progname : DEFAULT_PROGNAME);
}
