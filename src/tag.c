// Copyright (c) 2022 Nikita Zheleztsov
//
// Distributed under the GNU GPLv3 software license, see the accompanying
// file LICENSE or visit <https://www.gnu.org/licenses/gpl-3.0.en.html>

#include "tag.h"

#include <assert.h>
#include <stddef.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

// Allocates memory for tag and fills it with the data from string
tag_t *construct_tag(const char *string) {
  char *iter = NULL;
  char *end_iter = NULL;

  // puts iter to the '<', end_iter to the '>'
  if ((verify_tag(string, &iter, &end_iter)) != EXIT_SUCCESS) {
    return NULL;
  }

  // puts iter to the beggining of the name or whitespaces
  feature_t feature_buf = extract_feature(&iter);

  // puts iter to the
  char *name_buf = extract_name(&iter, end_iter, SPACE_MODE);
  if (!name_buf) {
    return NULL;
  }

  tag_t *tag = (tag_t *)malloc(sizeof(tag_t));
  if (!tag) {
    free(name_buf);
    return NULL;
  }

  tag->name = name_buf;
  tag->feature = feature_buf;
  tag->attr_arr.attributes = NULL;
  tag->attr_arr.attributes_num = 0;
  tag->attr_arr.attributes_cap = 0;

  if (tag->feature == OPENING) {
    attributes_arr_t temp_attr = {0, 0, NULL};
    if (extract_attributes(&temp_attr, iter, end_iter) != EXIT_SUCCESS) {
      free_tag(tag);
      return NULL;
    }

    tag->attr_arr = temp_attr;
  }
  return tag;
}

// Just checking if there's exactly one opening and closing bracket
// Extracting the beginning and the ending of the tag
// NOLINTNEXTLINE (bugprone-easily-swappable-parameters)
int verify_tag(const char *str, char **begin_iter, char **end_iter) {
  char *temp_begin_iter = strchr(str, '<');
  char *temp_end_iter = strchr(str, '>');

  {
    char *last_opening_bracket = strrchr(str, '<');
    char *last_closing_bracket = strrchr(str, '>');

    if (!temp_begin_iter || temp_begin_iter != last_opening_bracket ||
        !temp_end_iter || temp_end_iter != last_closing_bracket ||
        temp_end_iter - temp_begin_iter < 0) {
      return EXIT_FAILURE;
    }
  }

  (*begin_iter) = temp_begin_iter;
  (*end_iter) = temp_end_iter;
  return EXIT_SUCCESS;
}

// Extracting the tag's feature from the string and moving iterator to
// the beginning of the name (or to the beginning of the whitespaces if
// there's ones before the tag's name)
feature_t extract_feature(char **iter) {
  feature_t feature = (*(*iter + 1) == '/') ? CLOSING : OPENING;
  *iter = feature ? *iter + 2 : *iter + 1;
  return feature;
}

// Extracting the tag's name from the string
char *extract_name(char **iter, const char *const end_iter,
                   extract_name_mode_t mode) {
  char *begin_name = NULL;
  size_t name_size = find_name(iter, &begin_name, end_iter, mode);
  if (!name_size || !begin_name) {
    return NULL;
  }

  char *name = (char *)malloc((size_t)(name_size) + 1);
  if (!name) {
    return NULL;
  }

  strncpy(name, begin_name, (size_t)name_size);
  name[name_size] = '\0';
  return name;
}

// returns the size of the name
// begin_name - the pointer to the beginning of the name
// moves iterator to the next symbol after the end of the name
// in QUOTES_MODE the first symbol must be " or '
size_t find_name(char **iter, char **begin_name, const char *const end_iter,
                 extract_name_mode_t mode) {
  char *end_name = NULL;
  if (mode == SPACE_MODE) {
    // Skipping all spaces before the tag's name if there's ones
    while (**iter == ' ') {
      ++(*iter);
    }

    *begin_name = *iter;
    // Searching for the ending of the name
    while ((*iter) < end_iter && (((**iter) >= 'A' && (**iter) <= 'Z') ||
                                  ((**iter) >= 'a' && (**iter) <= 'z'))) {
      ++(*iter);
    }

    end_name = *iter;

  } else {
    // NOLINTNEXTLINE (*-id-dependent-backward-branch)
    const char to_find = ((**iter) == '"') ? '"' : '\'';
    *begin_name = ++(*iter);

    // NOLINTNEXTLINE (*-id-dependent-backward-branch)
    while ((*iter) < end_iter && (**iter) != to_find) {
      ++(*iter);
    }

    end_name = *iter;
    ++(*iter);
  }

  ptrdiff_t name_size = end_name - (*begin_name);
  if (name_size <= 0) {
    return 0;
  }

  return (size_t)name_size;
}

int extract_one_attr(attribute_t *attr, char **iter,
                     const char *const end_iter) {
  char *equal_sign = strchr(*iter, '=');
  if (!equal_sign) {
    return EXIT_FAILURE;
  }

  char *temp_name = extract_name(iter, equal_sign, SPACE_MODE);
  if (!temp_name) {
    return EXIT_FAILURE;
  }

  *iter = equal_sign + 1;
  while (**iter == ' ') {
    ++(*iter);
  }

  extract_name_mode_t mode =
      ((**iter) == '"' || (**iter) == '\'') ? QUOTES_MODE : SPACE_MODE;
  char *temp_value = extract_name(iter, end_iter, mode);
  if (!temp_value) {
    free(temp_name);
    return EXIT_FAILURE;
  }

  attr->attr_name = temp_name;
  attr->attr_value = temp_value;

  return EXIT_SUCCESS;
}

// NOLINTNEXTLINE (*-function-cognitive-complexity)
int extract_attributes(attributes_arr_t *arr, char *iter, char *end_iter) {
  assert(iter <= end_iter);

  // NOLINTNEXTLINE (*-id-dependent-backward-branch)
  while (iter != end_iter) {
    // Skipping the whitespaces
    // NOLINTNEXTLINE (*-id-dependent-backward-branch)
    while ((*iter) == ' ') {
      ++iter;
    }

    attribute_t attr = {NULL, NULL};
    // iter will be moved to the end if the attr in the string
    if ((extract_one_attr(&attr, &iter, end_iter)) != EXIT_SUCCESS) {
      free(arr->attributes);
      return EXIT_FAILURE;
    }

    // Reallocate if needed
    if ((arr->attributes_num + 1) > arr->attributes_cap) {
      size_t new_capacity = arr->attributes_cap ? arr->attributes_cap * 2 : 1;
      // if arr->attributes is NULL, the behavior of realloc
      // is the same as calling malloc (new_capacity)
      attribute_t *temp = (attribute_t *)realloc(
          arr->attributes, new_capacity * sizeof(attribute_t));

      if (temp) {
        arr->attributes = temp;
        arr->attributes_cap = new_capacity;
      } else {
        free(attr.attr_name);
        free(attr.attr_value);
        free(arr->attributes);
        return EXIT_FAILURE;
      }
    }

    arr->attributes[arr->attributes_num] = attr;
    ++arr->attributes_num;
    // iter = cut_before;
  }

  return EXIT_SUCCESS;
}

void print_json(FILE *out, tag_t *tag) {
  if (!out) {
    return;
  }

  fprintf(out, "{\n");
  if (tag) {
    fprintf(out, "\t\"name\": \"%s\",\n", tag->name);
    fprintf(out, "\t\"feature\": \"%s\",\n",
            tag->feature ? "closing" : "opening");

    if (tag->attr_arr.attributes_num) {
      fprintf(out, "\t\"attributes\": [\n");
      for (size_t i = 0; i < tag->attr_arr.attributes_num; ++i) {
        fprintf(out, "\t\t{\n");
        fprintf(out, "\t\t\t\"name\": \"%s\",\n",
                tag->attr_arr.attributes[i].attr_name);
        fprintf(out, "\t\t\t\"value\": \"%s\",\n",
                tag->attr_arr.attributes[i].attr_value);
        fprintf(out, "\t\t}%c\n",
                i + 1 != tag->attr_arr.attributes_num ? ',' : ' ');
      }

      fprintf(out, "\t]\n");
    }
  }

  fprintf(out, "}\n");
}

// Deallocating memory of the tag
void free_tag(tag_t *tag) {
  free(tag->name);
  for (size_t i = 0; i < tag->attr_arr.attributes_num; ++i) {
    free(tag->attr_arr.attributes[i].attr_name);
    free(tag->attr_arr.attributes[i].attr_value);
  }

  free(tag->attr_arr.attributes);
  free(tag);
}
