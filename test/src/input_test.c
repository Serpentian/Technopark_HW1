// Copyright (c) 2022 Nikita Zheleztsov
//
// Distributed under the GNU GPLv3 software license, see the accompanying
// file LICENSE or visit <https://www.gnu.org/licenses/gpl-3.0.en.html>

#include "input.h"

// The order is important here
#include "stdarg.h"
#include "stddef.h"
#include "stdint.h"
#include "setjmp.h"
#include "cmocka.h"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

static void eof_test(void **state) {
    // Arrange
    char input[] = "abcdefgh\0sdjfjdso";
    FILE *fmem = fmemopen(input, strlen(input), "r");

    // Act
    char *value = input_string(fmem);

    // Assert and clean
    assert_string_equal(value, "abcdefgh\0");
    fclose(fmem);
    free(value);
    (void) state;
}

static void endl_test(void **state) {
    // Arrange
    char input[] = "abcdefgh\nsdjfjdso";
    FILE *fmem = fmemopen(input, strlen(input), "r");

    // Act
    char *value = input_string(fmem);

    // Assert and clean
    assert_string_equal(value, "abcdefgh\0");
    fclose(fmem);
    free(value);
    (void) state;
}

int main() {
  const struct CMUnitTest tests[] = {
      cmocka_unit_test(eof_test),
      cmocka_unit_test(endl_test),
  };

  return cmocka_run_group_tests(tests, NULL, NULL);
}
