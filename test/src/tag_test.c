// Copyright (c) 2022 Nikita Zheleztsov
//
// Distributed under the GNU GPLv3 software license, see the accompanying
// file LICENSE or visit <https://www.gnu.org/licenses/gpl-3.0.en.html>

#include "tag.h"

#include <stdlib.h>

// The order is important here
#include "setjmp.h"
#include "cmocka.h"
#include "stdarg.h"
#include "stddef.h"
#include "stdint.h"

static void verify_test(void **state) {
  // Arrange
  char *iter_ok = NULL;
  char *end_iter_ok = NULL;
  char str_ok[] = "  < some kind of info >";

  char *iter_not_ok = NULL;
  char *end_iter_not_ok = NULL;
  char str_not_ok[] = "< some < kind of info >";

  // Act
  int status_ok = verify_tag(str_ok, &iter_ok, &end_iter_ok);
  int status_not_ok = verify_tag(str_not_ok, &iter_not_ok, &end_iter_not_ok);

  // Assert and clean
  assert_int_equal(status_ok, EXIT_SUCCESS);
  assert_ptr_equal(iter_ok, str_ok + 2);
  assert_ptr_equal(end_iter_ok, &str_ok[strlen(str_ok) - 1]);
  assert_int_equal(*end_iter_ok, '>');
  assert_int_equal(*iter_ok, '<');

  assert_int_equal(status_not_ok, EXIT_FAILURE);
  assert_null(iter_not_ok);
  assert_null(end_iter_not_ok);

  (void)state;
}

static void extract_feature_test(void **state) {
  // Arrange
  char closing[] = "</ ... >";
  char *iter_closing = closing;

  char opening[] = "< ... >";
  char *iter_opening = opening;

  // Act
  feature_t closing_feature = extract_feature(&iter_closing);
  feature_t opening_feature = extract_feature(&iter_opening);

  assert_int_equal(closing_feature, CLOSING);
  assert_int_equal(opening_feature, OPENING);

  assert_ptr_equal(iter_closing, closing + 2);
  assert_ptr_equal(iter_opening, opening + 1);
  assert_int_equal(*iter_opening, *iter_closing);
  assert_int_equal(*iter_opening, ' ');

  (void)state;
}

static void find_name_space_test(void **state) {
  char tag[] = "</  html>";
  const char *end = tag + strlen(tag) - 1;
  char *iter = tag + 2;

  char *begin_name = NULL;
  size_t size = find_name(&iter, &begin_name, end, SPACE_MODE);

  assert_int_equal(size, 4);
  assert_ptr_equal(iter, end);

  (void)state;
}

static void find_name_quote_test(void **state) {
  char tag[] = "'text with spaces'";
  const char *end = tag + strlen(tag) - 1;
  char *iter = tag;

  char *begin_name = NULL;
  size_t size = find_name(&iter, &begin_name, end, QUOTES_MODE);

  assert_int_equal(size, strlen(tag) - 2);
  assert_ptr_equal(begin_name, tag + 1);

  (void)state;
}

static void extract_attributes_test(void **state) {
  attributes_arr_t temp_attr = {0, 0, NULL};
  char attributes[] = "  class='info with spaces'   id=ID>";
  int err = extract_attributes(&temp_attr, attributes,
                     attributes + strlen(attributes) - 1);

  assert_int_equal(err, EXIT_SUCCESS);
  assert_int_equal(temp_attr.attributes_num, 2);
  assert_string_equal(temp_attr.attributes->attr_name, "class");
  assert_string_equal(temp_attr.attributes->attr_value, "info with spaces");
  assert_string_equal(temp_attr.attributes[1].attr_name, "id");
  assert_string_equal(temp_attr.attributes[1].attr_value, "ID");

  for (size_t i = 0; i < temp_attr.attributes_num; ++i) {
      free(temp_attr.attributes[i].attr_name);
      free(temp_attr.attributes[i].attr_value);
  }

  free(temp_attr.attributes);
  (void) state;
}

static void construct_tag_test(void **state) {
  char tag_string[] =
      "<span id = \"ID0EVB\" style=\"cursor:\
url(https://cdn.reverso.net/), auto;\" direction=\"targettarget\">";

  tag_t *tag = construct_tag(tag_string);

  assert_true(tag);
  assert_string_equal(tag->name, "span");
  assert_int_equal(tag->feature, OPENING);

  assert_int_equal(tag->attr_arr.attributes_num, 3);

  assert_string_equal(tag->attr_arr.attributes[0].attr_name, "id");
  assert_string_equal(tag->attr_arr.attributes[0].attr_value, "ID0EVB");

  assert_string_equal(tag->attr_arr.attributes[1].attr_name, "style");
  assert_string_equal(tag->attr_arr.attributes[1].attr_value, "cursor:\
url(https://cdn.reverso.net/), auto;");

  assert_string_equal(tag->attr_arr.attributes[2].attr_name, "direction");
  assert_string_equal(tag->attr_arr.attributes[2].attr_value, "targettarget");

  free_tag(tag);
  (void)state;
}

int main() {
  const struct CMUnitTest tests[] = {
      cmocka_unit_test(verify_test),
      cmocka_unit_test(extract_feature_test),
      cmocka_unit_test(find_name_space_test),
      cmocka_unit_test(find_name_quote_test),
      cmocka_unit_test(extract_attributes_test),
      cmocka_unit_test(construct_tag_test),
  };

  return cmocka_run_group_tests(tests, NULL, NULL);
}
